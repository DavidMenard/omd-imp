<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-23
 * Time: 7:52 PM
 */

namespace Inovva\omdimp;

use Inovva\Logger\Logger;
use Inovva\omdimp\Helper\Exception\APIException;
use Inovva\omdimp\Helper\JsonMapper;
use Inovva\omdimp\Helper\JsonRequest;
use Inovva\omdimp\Models\In\AuthenticateInput;
use Inovva\omdimp\Models\Out\Authenticate\AuthenticationResponseOutput;
use Inovva\omdimp\Models\Out\Error\ErrorOutput;
use Inovva\omdimp\Models\Out\Error\ErrorResponseOutput;

class ValidateAuthenticate
{
    /**
     * @param string $username
     * @param string $password
     * @return bool|int
     * @throws Helper\Exception\JsonMapperException
     */
    public static function ProcessAuthenticate($username, $password)
    {
        $authModel = new AuthenticateInput($username, $password);

        try {
            $validate = JsonRequest::getInstance(Logger::getInstance())->call("/webservices/rest/brand/" . Config::BRAND . "/authentication/validate/*", $authModel->toJSON(), "POST");
            /** @var AuthenticationResponseOutput $authenticationResponse */
            $authenticationResponse = JsonMapper::getInstance(Logger::getInstance())->map($validate, new AuthenticationResponseOutput());

            $responseInfo = array_pop($authenticationResponse->ResponseInfo);
            return $responseInfo->OmedaCustomerId;
        } catch (APIException $authExeption) {
            $errorObj = JsonMapper::getInstance(Logger::getInstance())->map(json_decode($authExeption->getMessage()), new ErrorResponseOutput());
            /** @var ErrorOutput $error */
            foreach ($errorObj->Errors as $error) {
                Logger::getInstance()->error("OMEDA - ERROR - " . $error->Error . " for user $username");
            }

            return false;
        } catch (\Exception $exception) {
            Logger::getInstance()->error("OMEDA - ERROR - " . $exception->getMessage() . " for user $username");
            return false;
        }
    }
}