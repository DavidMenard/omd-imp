<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2016-05-03
 * Time: 11:48
 * API V2/V3
 */

namespace Inovva\omdimp\Models\In;

class AuthenticateInput
{
    /** @var string $Username  */
    private $Username;

    /** @var string $Password  */
    private $Password;

    /** @var string $ExternalCustomerIdNamespace */
    private $ExternalCustomerIdNamespace = OMEDA_EXTERNAL_NAMESPACE;

    /**
     * AuthenticateInput constructor.
     * @param string $Username
     * @param string $Password
     */
    public function __construct($Username, $Password)
    {
        $this->Username = $Username;
        $this->Password = $Password;
    }

    /**
     * @return array representation of the class member
     */
    public function toArray()
    {
        $data = array();

        $data["Username"] = $this->Username;
        $data["Password"] = $this->Password;
        $data["ExternalCustomerIdNamespace"] = $this->ExternalCustomerIdNamespace;

        return $data;
    }

    function toJSON()
    {
        return json_encode($this->toArray());
    }
}