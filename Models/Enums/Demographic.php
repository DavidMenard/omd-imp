<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-30
 * Time: 2:17 PM
 */

namespace Inovva\omdimp\Models\Enums;



use Inovva\omdimp\Models\Out\Brand\DemographicOutput;
use Inovva\omdimp\Models\Out\CustomerDemographicOutput;
use Inovva\omdimp\Models\Out\CustomerLookupOutput;

final class Demographic
{
    const GENDER                    = 46;
    const AREA_OF_RESPONSABILITY    = 52;
    const PROVINCE                  = 53;
    const MANAGEMENT_LEVEL          = 54;
    const NURSE_TYPE                = 55;
    const PHARMACY_JOB_FUNCTION     = 57;
    const SPECIALITY_TYPE           = 59;
    const STUDY_PROVINCE            = 60;
    const YEAR_OF_GRADUATION        = 61;
    const PRIVACY_POLICY            = 72;
    const SCREEN_NAME               = 73;
    const LICENCE_NUMBER            = 77;
    const USER_KEY                  = 78;
    const PERSONA                   = 568;
    const LANGUAGE                  = 569;
    const SUBPERSONA                = 570;
    const ACCESS_LEVEL              = 572;
    const BUSINESS_TYPE             = 573;
    const PHARMACY_GROUP            = 574;

    /**
     * Receive a list of demographic and find the
     *
     * @param $demographicId
     * @param CustomerLookupOutput $customer
     * @return CustomerDemographicOutput|null
     */
    private static function getDemographicValue($demographicId, CustomerLookupOutput $customer)
    {

        $customerDemographicList = $customer->getCustomerDemographics();
        if (isset($customerDemographicList[$demographicId])) {
            return $customerDemographicList[$demographicId];
        }

        return null;
    }

    /**
     * @param $demographicId
     * @param CustomerLookupOutput $customer
     * @param DemographicOutput[] $demographicList
     * @param string $field
     * @return null
     */
    public static function getDemographicDescription($demographicId, CustomerLookupOutput $customer, $demographicList, $field = 'Description')
    {
        if (!isset($demographicList[$demographicId])) {
            return null;
        }

        /** @var CustomerDemographicOutput $myDemographic */
        $myDemographic = self::getDemographicValue($demographicId, $customer);

        switch ($myDemographic->DemographicType) {
            case 1:
            case 2:
                foreach($demographicList[$demographicId]->DemographicValues as $demographicValue) {
                    if ($demographicValue->Id === $myDemographic->ValueId) {
                        return $demographicValue->{$field};
                    }
                }
                break;
            case 3:
                return $myDemographic->ValueText;
                break;

        }

        return null;
    }

    /**
     * @param DemographicOutput[] $demographicList
     * @param string $index
     * @return DemographicOutput[]
     */
    public static function sortDemographics($demographicList, $index = 'Id')
    {
        /** @var DemographicOutput[] $demographicsByKey */
        $demographicsByKey = array();
        foreach ($demographicList as $demographic) {
            $demographicsByKey[$demographic->{$index}] = $demographic;
        }

        return $demographicsByKey;
    }

}