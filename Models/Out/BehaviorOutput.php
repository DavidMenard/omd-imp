<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class BehaviorOutput
{
    /** @var integer $BehaviorId */
    private $BehaviorId;

    /** @var string $FirstOccurenceDate */
    private $FirstOccurenceDate;

    /** @var string $LastOccurenceDate */
    private $LastOccurenceDate;

    /** @var integer $NumberOfOccurrences */
    private $NumberOfOccurrences;

    /** @var string $PromoCode */
    private $PromoCode;

    /**
     * @return int
     */
    public function getBehaviorId()
    {
        return $this->BehaviorId;
    }

    /**
     * @param int $BehaviorId
     */
    public function setBehaviorId($BehaviorId)
    {
        $this->BehaviorId = $BehaviorId;
    }

    /**
     * @return string
     */
    public function getFirstOccurenceDate()
    {
        return $this->FirstOccurenceDate;
    }

    /**
     * @param string $FirstOccurenceDate
     */
    public function setFirstOccurenceDate($FirstOccurenceDate)
    {
        $this->FirstOccurenceDate = $FirstOccurenceDate;
    }

    /**
     * @return string
     */
    public function getLastOccurenceDate()
    {
        return $this->LastOccurenceDate;
    }

    /**
     * @param string $LastOccurenceDate
     */
    public function setLastOccurenceDate($LastOccurenceDate)
    {
        $this->LastOccurenceDate = $LastOccurenceDate;
    }

    /**
     * @return int
     */
    public function getNumberOfOccurrences()
    {
        return $this->NumberOfOccurrences;
    }

    /**
     * @param int $NumberOfOccurrences
     */
    public function setNumberOfOccurrences($NumberOfOccurrences)
    {
        $this->NumberOfOccurrences = $NumberOfOccurrences;
    }

    /**
     * @return string
     */
    public function getPromoCode()
    {
        return $this->PromoCode;
    }

    /**
     * @param string $PromoCode
     */
    public function setPromoCode($PromoCode)
    {
        $this->PromoCode = $PromoCode;
    }
}
