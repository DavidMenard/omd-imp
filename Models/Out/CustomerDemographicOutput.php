<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class CustomerDemographicOutput
{
    /** @var string $Id */
    public $Id;

    /** @var integer $DemographicId */
    public $DemographicId;

    /** @var integer $DemographicType */
    public $DemographicType;

    /** @var integer $DemographicAge */
    public $DemographicAge;

    /** @var integer $ValueId */
    public $ValueId;

    /** @var string $ValueText */
    public $ValueText;

    /** @var string $ValueDate */
    public $ValueDate;

    /** @var string $WriteInDesc */
    public $WriteInDesc;

    /** @var string $AlternateId */
    public $AlternateId;

    /** @var string $ChangedDate */
    public $ChangedDate;
}
