<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class EmailOutput
{
    /** @var string $Id */
    private $Id;

    /** @var integer $EmailContactType */
    private $EmailContactType;

    /** @var string $EmailAddress */
    private $EmailAddress;

    /** @var string $ChangedDate */
    private $ChangedDate;

    /** @var int $StatusCode */
    private $StatusCode;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param string $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return int
     */
    public function getEmailContactType()
    {
        return $this->EmailContactType;
    }

    /**
     * @param int $EmailContactType
     */
    public function setEmailContactType($EmailContactType)
    {
        $this->EmailContactType = $EmailContactType;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->EmailAddress;
    }

    /**
     * @param string $EmailAddress
     */
    public function setEmailAddress($EmailAddress)
    {
        $this->EmailAddress = $EmailAddress;
    }

    /**
     * @return string
     */
    public function getChangedDate()
    {
        return $this->ChangedDate;
    }

    /**
     * @param string $ChangedDate
     */
    public function setChangedDate($ChangedDate)
    {
        $this->ChangedDate = $ChangedDate;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    /**
     * @param int $StatusCode
     */
    public function setStatusCode($StatusCode)
    {
        $this->StatusCode = $StatusCode;
    }
}
