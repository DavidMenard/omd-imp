<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class CustomerLookupOutput
{
    /** @var int $Id */
    private $Id;

    /** @var integer $ReaderId */
    private $ReaderId;

    /** @var string $Salutation */
    private $Salutation;

    /** @var string $FirstName */
    private $FirstName;

    /** @var string $MiddleName */
    private $MiddleName;

    /** @var string $LastName */
    private $LastName;

    /** @var string $Suffix */
    private $Suffix;

    /** @var string $Title */
    private $Title;

    /** @var string $Gender */
    private $Gender;

    /** @var string $ClientCustomerId */
    private $ClientCustomerId;

    /** @var string $OriginalPromoCode */
    private $OriginalPromoCode;

    /** @var string $PromoCode */
    private $PromoCode;

    /** @var string $SignUpDate */
    private $SignUpDate;

    /** @var string $ChangedDate */
    private $ChangedDate;

    /** @var string $GlobalMinChangedDate */
    private $GlobalMinChangedDate;

    /** @var string $GlobalMaxChangedDate */
    private $GlobalMaxChangedDate;

    /** @var integer $StatusCode */
    private $StatusCode;

    /** @var boolean $HasMergeHistory */
    private $HasMergeHistory;

    /** @var string $Customer */
    private $Customer;

    /** @var AddressOutput[]|null $Addresses */
    private $Addresses;

    /** @var PhoneNumberOutput[]|null $PhoneNumbers */
    private $PhoneNumbers;

    /** @var EmailOutput[]|null $Emails */
    private $Emails;

    /** @var CustomerDemographicOutput[]|null $CustomerDemographics */
    private $CustomerDemographics;

    /** @var SubscriptionOutput[]|null $Subscriptions */
    private $Subscriptions;

    /** @var BehaviorOutput[]|null $Behaviors */
    private $Behaviors;

    /** @var ExternalIdentifierOutput[]|null $ExternalIds */
    private $ExternalIds;

    /** @var ProductOutput[]|null $Products */
    private $Products;

    /** @var string $EncryptedCustomerId */
    private $EncryptedCustomerId;

    /** @var int $CustomerStatusId */
    private $CustomerStatusId;

    /** @var string $SubmissionId */
    private $SubmissionId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return int
     */
    public function getReaderId()
    {
        return $this->ReaderId;
    }

    /**
     * @param int $ReaderId
     */
    public function setReaderId($ReaderId)
    {
        $this->ReaderId = $ReaderId;
    }

    /**
     * @return string
     */
    public function getSalutation()
    {
        return $this->Salutation;
    }

    /**
     * @param string $Salutation
     */
    public function setSalutation($Salutation)
    {
        $this->Salutation = $Salutation;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->FirstName;
    }

    /**
     * @param string $FirstName
     */
    public function setFirstName($FirstName)
    {
        $this->FirstName = $FirstName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->MiddleName;
    }

    /**
     * @param string $MiddleName
     */
    public function setMiddleName($MiddleName)
    {
        $this->MiddleName = $MiddleName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->LastName;
    }

    /**
     * @param string $LastName
     */
    public function setLastName($LastName)
    {
        $this->LastName = $LastName;
    }

    /**
     * @return string
     */
    public function getSuffix()
    {
        return $this->Suffix;
    }

    /**
     * @param string $Suffix
     */
    public function setSuffix($Suffix)
    {
        $this->Suffix = $Suffix;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->Title;
    }

    /**
     * @param string $Title
     */
    public function setTitle($Title)
    {
        $this->Title = $Title;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->Gender;
    }

    /**
     * @param string $Gender
     */
    public function setGender($Gender)
    {
        $this->Gender = $Gender;
    }

    /**
     * @return string
     */
    public function getClientCustomerId()
    {
        return $this->ClientCustomerId;
    }

    /**
     * @param string $ClientCustomerId
     */
    public function setClientCustomerId($ClientCustomerId)
    {
        $this->ClientCustomerId = $ClientCustomerId;
    }

    /**
     * @return string
     */
    public function getOriginalPromoCode()
    {
        return $this->OriginalPromoCode;
    }

    /**
     * @param string $OriginalPromoCode
     */
    public function setOriginalPromoCode($OriginalPromoCode)
    {
        $this->OriginalPromoCode = $OriginalPromoCode;
    }

    /**
     * @return string
     */
    public function getPromoCode()
    {
        return $this->PromoCode;
    }

    /**
     * @param string $PromoCode
     */
    public function setPromoCode($PromoCode)
    {
        $this->PromoCode = $PromoCode;
    }

    /**
     * @return string
     */
    public function getSignUpDate()
    {
        return $this->SignUpDate;
    }

    /**
     * @param string $SignUpDate
     */
    public function setSignUpDate($SignUpDate)
    {
        $this->SignUpDate = $SignUpDate;
    }

    /**
     * @return string
     */
    public function getChangedDate()
    {
        return $this->ChangedDate;
    }

    /**
     * @param string $ChangedDate
     */
    public function setChangedDate($ChangedDate)
    {
        $this->ChangedDate = $ChangedDate;
    }

    /**
     * @return string
     */
    public function getGlobalMinChangedDate()
    {
        return $this->GlobalMinChangedDate;
    }

    /**
     * @param string $GlobalMinChangedDate
     */
    public function setGlobalMinChangedDate($GlobalMinChangedDate)
    {
        $this->GlobalMinChangedDate = $GlobalMinChangedDate;
    }

    /**
     * @return string
     */
    public function getGlobalMaxChangedDate()
    {
        return $this->GlobalMaxChangedDate;
    }

    /**
     * @param string $GlobalMaxChangedDate
     */
    public function setGlobalMaxChangedDate($GlobalMaxChangedDate)
    {
        $this->GlobalMaxChangedDate = $GlobalMaxChangedDate;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    /**
     * @param int $StatusCode
     */
    public function setStatusCode($StatusCode)
    {
        $this->StatusCode = $StatusCode;
    }

    /**
     * @return bool
     */
    public function isHasMergeHistory()
    {
        return $this->HasMergeHistory;
    }

    /**
     * @param bool $HasMergeHistory
     */
    public function setHasMergeHistory($HasMergeHistory)
    {
        $this->HasMergeHistory = $HasMergeHistory;
    }

    /**
     * @return string
     */
    public function getCustomer()
    {
        return $this->Customer;
    }

    /**
     * @param string $Customer
     */
    public function setCustomer($Customer)
    {
        $this->Customer = $Customer;
    }

    /**
     * @return AddressOutput[]|null
     */
    public function getAddresses()
    {
        return $this->Addresses;
    }

    /**
     * @param AddressOutput[]|null $Addresses
     */
    public function setAddresses($Addresses)
    {
        $this->Addresses = $Addresses;
    }

    /**
     * @return PhoneNumberOutput[]|null
     */
    public function getPhoneNumbers()
    {
        return $this->PhoneNumbers;
    }

    /**
     * @param PhoneNumberOutput[]|null $PhoneNumbers
     */
    public function setPhoneNumbers($PhoneNumbers)
    {
        $this->PhoneNumbers = $PhoneNumbers;
    }

    /**
     * @return EmailOutput[]|null
     */
    public function getEmails()
    {
        return $this->Emails;
    }

    /**
     * @param EmailOutput[]|null $Emails
     */
    public function setEmails($Emails)
    {
        $this->Emails = $Emails;
    }

    /**
     * @return CustomerDemographicOutput[]|null
     */
    public function getCustomerDemographics()
    {
        return $this->CustomerDemographics;
    }

    /**
     * @param CustomerDemographicOutput[]|null $CustomerDemographics
     */
    public function setCustomerDemographics($CustomerDemographics)
    {
        $this->CustomerDemographics = $CustomerDemographics;
    }

    /**
     * @return SubscriptionOutput[]|null
     */
    public function getSubscriptions()
    {
        return $this->Subscriptions;
    }

    /**
     * @param SubscriptionOutput[]|null $Subscriptions
     */
    public function setSubscriptions($Subscriptions)
    {
        $this->Subscriptions = $Subscriptions;
    }

    /**
     * @return BehaviorOutput[]|null
     */
    public function getBehaviors()
    {
        return $this->Behaviors;
    }

    /**
     * @param BehaviorOutput[]|null $Behaviors
     */
    public function setBehaviors($Behaviors)
    {
        $this->Behaviors = $Behaviors;
    }

    /**
     * @return ExternalIdentifierOutput[]|null
     */
    public function getExternalIds()
    {
        return $this->ExternalIds;
    }

    /**
     * @param ExternalIdentifierOutput[]|null $ExternalIds
     */
    public function setExternalIds($ExternalIds)
    {
        $this->ExternalIds = $ExternalIds;
    }

    /**
     * @return ProductOutput[]|null
     */
    public function getProducts()
    {
        return $this->Products;
    }

    /**
     * @param ProductOutput[]|null $Products
     */
    public function setProducts($Products)
    {
        $this->Products = $Products;
    }

    /**
     * @return string
     */
    public function getEncryptedCustomerId()
    {
        return $this->EncryptedCustomerId;
    }

    /**
     * @param string $EncryptedCustomerId
     */
    public function setEncryptedCustomerId($EncryptedCustomerId)
    {
        $this->EncryptedCustomerId = $EncryptedCustomerId;
    }

    /**
     * @return int
     */
    public function getCustomerStatusId()
    {
        return $this->CustomerStatusId;
    }

    /**
     * @param int $CustomerStatusId
     */
    public function setCustomerStatusId($CustomerStatusId)
    {
        $this->CustomerStatusId = $CustomerStatusId;
    }

    /**
     * @return string
     */
    public function getSubmissionId()
    {
        return $this->SubmissionId;
    }

    /**
     * @param string $SubmissionId
     */
    public function setSubmissionId($SubmissionId)
    {
        $this->SubmissionId = $SubmissionId;
    }
}
