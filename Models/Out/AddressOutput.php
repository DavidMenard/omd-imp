<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class AddressOutput
{
    /** @var string $Id */
    private $Id;

    /** @var integer $AddressContactType */
    private $AddressContactType;

    /** @var string $Company */
    private $Company;

    /** @var string $Street */
    private $Street;

    /** @var string $ApartmentMailStop */
    private $ApartmentMailStop;

    /** @var string $ExtraAddress */
    private $ExtraAddress;

    /** @var string $City */
    private $City;

    /** @var string $RegionCode */
    private $RegionCode;

    /** @var string $Region */
    private $Region;

    /** @var string $PostalCode */
    private $PostalCode;

    /** @var string $CountryCode */
    private $CountryCode;

    /** @var string $Country */
    private $Country;

    /** @var string $ChangedDate */
    private $ChangedDate;

    /** @var int $StatusCode */
    private $StatusCode;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param string $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return int
     */
    public function getAddressContactType()
    {
        return $this->AddressContactType;
    }

    /**
     * @param int $AddressContactType
     */
    public function setAddressContactType($AddressContactType)
    {
        $this->AddressContactType = $AddressContactType;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->Company;
    }

    /**
     * @param string $Company
     */
    public function setCompany($Company)
    {
        $this->Company = $Company;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->Street;
    }

    /**
     * @param string $Street
     */
    public function setStreet($Street)
    {
        $this->Street = $Street;
    }

    /**
     * @return string
     */
    public function getApartmentMailStop()
    {
        return $this->ApartmentMailStop;
    }

    /**
     * @param string $ApartmentMailStop
     */
    public function setApartmentMailStop($ApartmentMailStop)
    {
        $this->ApartmentMailStop = $ApartmentMailStop;
    }

    /**
     * @return string
     */
    public function getExtraAddress()
    {
        return $this->ExtraAddress;
    }

    /**
     * @param string $ExtraAddress
     */
    public function setExtraAddress($ExtraAddress)
    {
        $this->ExtraAddress = $ExtraAddress;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * @param string $City
     */
    public function setCity($City)
    {
        $this->City = $City;
    }

    /**
     * @return string
     */
    public function getRegionCode()
    {
        return $this->RegionCode;
    }

    /**
     * @param string $RegionCode
     */
    public function setRegionCode($RegionCode)
    {
        $this->RegionCode = $RegionCode;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->Region;
    }

    /**
     * @param string $Region
     */
    public function setRegion($Region)
    {
        $this->Region = $Region;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->PostalCode;
    }

    /**
     * @param string $PostalCode
     */
    public function setPostalCode($PostalCode)
    {
        $this->PostalCode = $PostalCode;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->CountryCode;
    }

    /**
     * @param string $CountryCode
     */
    public function setCountryCode($CountryCode)
    {
        $this->CountryCode = $CountryCode;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * @param string $Country
     */
    public function setCountry($Country)
    {
        $this->Country = $Country;
    }

    /**
     * @return string
     */
    public function getChangedDate()
    {
        return $this->ChangedDate;
    }

    /**
     * @param string $ChangedDate
     */
    public function setChangedDate($ChangedDate)
    {
        $this->ChangedDate = $ChangedDate;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    /**
     * @param int $StatusCode
     */
    public function setStatusCode($StatusCode)
    {
        $this->StatusCode = $StatusCode;
    }
}
