<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-03
 * Time: 11:53 AM
 */

namespace Inovva\omdimp\Models\Out\Brand;


class BrandOutput
{
    /** @var integer $Id */
    private $Id;

    /** @var string $Description */
    private $Description;

    /** @var string $BrandAbbrev */
    private $BrandAbbrev;

    /** @var integer $CustomerCount */
    private $CustomerCount;

    /** @var DemographicOutput[]|null $Demographics */
    private $Demographics;

    /** @var ProductOutput[]|null $Products */
    private $Products;

    /** @var ContactTypeOutput[]|null $ContactTypes */
    private $ContactTypes;

    /** @var DeploymentTypeOutput[]|null $DeploymentTypes */
    private $DeploymentTypes;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param string $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return string
     */
    public function getBrandAbbrev()
    {
        return $this->BrandAbbrev;
    }

    /**
     * @param string $BrandAbbrev
     */
    public function setBrandAbbrev($BrandAbbrev)
    {
        $this->BrandAbbrev = $BrandAbbrev;
    }

    /**
     * @return int
     */
    public function getCustomerCount()
    {
        return $this->CustomerCount;
    }

    /**
     * @param int $CustomerCount
     */
    public function setCustomerCount($CustomerCount)
    {
        $this->CustomerCount = $CustomerCount;
    }

    /**
     * @return DemographicOutput[]|null
     */
    public function getDemographics()
    {
        return $this->Demographics;
    }

    /**
     * @param DemographicOutput[]|null $Demographics
     */
    public function setDemographics($Demographics)
    {
        $this->Demographics = $Demographics;
    }

    /**
     * @return ProductOutput[]|null
     */
    public function getProducts()
    {
        return $this->Products;
    }

    /**
     * @param ProductOutput[]|null $Products
     */
    public function setProducts($Products)
    {
        $this->Products = $Products;
    }

    /**
     * @return ContactTypeOutput[]|null
     */
    public function getContactTypes()
    {
        return $this->ContactTypes;
    }

    /**
     * @param ContactTypeOutput[]|null $ContactTypes
     */
    public function setContactTypes($ContactTypes)
    {
        $this->ContactTypes = $ContactTypes;
    }

    /**
     * @return DeploymentTypeOutput[]|null
     */
    public function getDeploymentTypes()
    {
        return $this->DeploymentTypes;
    }

    /**
     * @param DeploymentTypeOutput[]|null $DeploymentTypes
     */
    public function setDeploymentTypes($DeploymentTypes)
    {
        $this->DeploymentTypes = $DeploymentTypes;
    }
}