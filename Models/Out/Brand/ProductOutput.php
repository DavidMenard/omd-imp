<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-03
 * Time: 11:53 AM
 */

namespace Inovva\omdimp\Models\Out\Brand;


class ProductOutput
{
    /** @var integer $Id */
    private $Id;

    /** @var string $Description */
    private $Description;

    /** @var integer $ProductType */
    private $ProductType;

    /** @var string $AlternateId */
    private $AlternateId;

    /** @var integer $Frequency */
    private $Frequency;

    /** @var string $FrequencyType */
    private $FrequencyType;

    /** @var integer $DeploymentTypeId */
    private $DeploymentTypeId;

    /** @var MarketingClassOutput[]|null $MarketingClasses */
    private $MarketingClasses;

    /** @var IssueOutput[]|null $Issues */
    private $Issues;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param string $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return int
     */
    public function getProductType()
    {
        return $this->ProductType;
    }

    /**
     * @param int $ProductType
     */
    public function setProductType($ProductType)
    {
        $this->ProductType = $ProductType;
    }

    /**
     * @return string
     */
    public function getAlternateId()
    {
        return $this->AlternateId;
    }

    /**
     * @param string $AlternateId
     */
    public function setAlternateId($AlternateId)
    {
        $this->AlternateId = $AlternateId;
    }

    /**
     * @return int
     */
    public function getFrequency()
    {
        return $this->Frequency;
    }

    /**
     * @param int $Frequency
     */
    public function setFrequency($Frequency)
    {
        $this->Frequency = $Frequency;
    }

    /**
     * @return string
     */
    public function getFrequencyType()
    {
        return $this->FrequencyType;
    }

    /**
     * @param string $FrequencyType
     */
    public function setFrequencyType($FrequencyType)
    {
        $this->FrequencyType = $FrequencyType;
    }

    /**
     * @return int
     */
    public function getDeploymentTypeId()
    {
        return $this->DeploymentTypeId;
    }

    /**
     * @param int $DeploymentTypeId
     */
    public function setDeploymentTypeId($DeploymentTypeId)
    {
        $this->DeploymentTypeId = $DeploymentTypeId;
    }

    /**
     * @return MarketingClassOutput[]|null
     */
    public function getMarketingClasses()
    {
        return $this->MarketingClasses;
    }

    /**
     * @param MarketingClassOutput[]|null $MarketingClasses
     */
    public function setMarketingClasses($MarketingClasses)
    {
        $this->MarketingClasses = $MarketingClasses;
    }

    /**
     * @return IssueOutput[]|null
     */
    public function getIssues()
    {
        return $this->Issues;
    }

    /**
     * @param IssueOutput[]|null $Issues
     */
    public function setIssues($Issues)
    {
        $this->Issues = $Issues;
    }
}