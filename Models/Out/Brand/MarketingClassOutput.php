<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-03
 * Time: 11:53 AM
 */

namespace Inovva\omdimp\Models\Out\Brand;


class MarketingClassOutput
{
    /** @var integer $Id */
    private $Id;

    /** @var string $Description */
    private $Description;

    /** @var string $ShortDescription */
    private $ShortDescription;

    /** @var string $ClassId */
    private $ClassId;

    /** @var integer $StatusCode */
    private $StatusCode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param string $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->ShortDescription;
    }

    /**
     * @param string $ShortDescription
     */
    public function setShortDescription($ShortDescription)
    {
        $this->ShortDescription = $ShortDescription;
    }

    /**
     * @return string
     */
    public function getClassId()
    {
        return $this->ClassId;
    }

    /**
     * @param string $ClassId
     */
    public function setClassId($ClassId)
    {
        $this->ClassId = $ClassId;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    /**
     * @param int $StatusCode
     */
    public function setStatusCode($StatusCode)
    {
        $this->StatusCode = $StatusCode;
    }
}