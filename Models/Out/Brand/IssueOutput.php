<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-03
 * Time: 11:53 AM
 */

namespace Inovva\omdimp\Models\Out\Brand;


class IssueOutput
{
    /** @var integer $Id */
    private $Id;

    /** @var string $Description */
    private $Description;

    /** @var string $IssueDate */
    private $IssueDate;

    /** @var string $AlternateId */
    private $AlternateId;

    /** @var integer $StatusCode */
    private $StatusCode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param string $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return string
     */
    public function getIssueDate()
    {
        return $this->IssueDate;
    }

    /**
     * @param string $IssueDate
     */
    public function setIssueDate($IssueDate)
    {
        $this->IssueDate = $IssueDate;
    }

    /**
     * @return string
     */
    public function getAlternateId()
    {
        return $this->AlternateId;
    }

    /**
     * @param string $AlternateId
     */
    public function setAlternateId($AlternateId)
    {
        $this->AlternateId = $AlternateId;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    /**
     * @param int $StatusCode
     */
    public function setStatusCode($StatusCode)
    {
        $this->StatusCode = $StatusCode;
    }
}