<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-03
 * Time: 11:53 AM
 */

namespace Inovva\omdimp\Models\Out\Brand;


class DemographicOutput
{
    /** @var integer $Id */
    public $Id;

    /** @var string $Description */
    public $Description;

    /** @var string $DemoLegacyId */
    public $DemoLegacyId;

    /** @var integer $DemographicType */
    public $DemographicType;

    /** @var DemographicValueOutput[]|null $DemographicValues */
    public $DemographicValues;

    /** @var integer[]|null $AuditedProducts */
    public $AuditedProducts;

    /** @var integer $DemographicAge */
    public $DemographicAge;

    /** @var boolean $Audited */
    public $Audited;

    /** @var string $WebformText */
    public $OmedaWebformText;

    /** @var integer $OmedaWebformViewCode */
    public $OmedaWebformViewCode;

    /** @var integer $OmedaWebformSequence */
    public $OmedaWebformSequence;
}