<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-03
 * Time: 11:53 AM
 */

namespace Inovva\omdimp\Models\Out\Brand;


class DemographicValueOutput
{
    /** @var integer $Id */
    public $Id;

    /** @var string $Description */
    public $Description;

    /** @var string $ShortDescription */
    public $ShortDescription;

    /** @var integer $DemographicValueType */
    public $DemographicValueType;

    /** @var integer $Sequence */
    public $Sequence;

    /** @var string $AlternateId */
    public $AlternateId;

    /** @var string $WebformText */
    public $OmedaWebformText;

    /** @var integer $OmedaWebformViewCode */
    public $OmedaWebformViewCode;

    /** @var integer $OmedaWebformSequence */
    public $OmedaWebformSequence;
}