<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-03
 * Time: 11:53 AM
 */

namespace Inovva\omdimp\Models\Out\Brand;


class DeploymentTypeOutput
{
    /** @var integer $Id */
    private $Id;

    /** @var string $Name */
    private $Name;

    /** @var string $Description */
    private $Description;

    /** @var string $LongDescription */
    private $LongDescription;

    /** @var string $AlternateId */
    private $AlternateId;

    /** @var integer $StatusCode */
    private $StatusCode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param string $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return string
     */
    public function getLongDescription()
    {
        return $this->LongDescription;
    }

    /**
     * @param string $LongDescription
     */
    public function setLongDescription($LongDescription)
    {
        $this->LongDescription = $LongDescription;
    }

    /**
     * @return string
     */
    public function getAlternateId()
    {
        return $this->AlternateId;
    }

    /**
     * @param string $AlternateId
     */
    public function setAlternateId($AlternateId)
    {
        $this->AlternateId = $AlternateId;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    /**
     * @param int $StatusCode
     */
    public function setStatusCode($StatusCode)
    {
        $this->StatusCode = $StatusCode;
    }
}