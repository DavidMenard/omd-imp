<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class ProductOutput
{
    /** @var int $DataFieldId */
    private $DataFieldId;

    /** @var string $ValueText */
    private $ValueText;

    /** @var string $ValueDate */
    private $ValueDate;

    /**
     * @return int
     */
    public function getDataFieldId()
    {
        return $this->DataFieldId;
    }

    /**
     * @param int $DataFieldId
     */
    public function setDataFieldId($DataFieldId)
    {
        $this->DataFieldId = $DataFieldId;
    }

    /**
     * @return string
     */
    public function getValueText()
    {
        return $this->ValueText;
    }

    /**
     * @param string $ValueText
     */
    public function setValueText($ValueText)
    {
        $this->ValueText = $ValueText;
    }

    /**
     * @return string
     */
    public function getValueDate()
    {
        return $this->ValueDate;
    }

    /**
     * @param string $ValueDate
     */
    public function setValueDate($ValueDate)
    {
        $this->ValueDate = $ValueDate;
    }
}