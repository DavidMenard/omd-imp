<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class ExternalIdentifierOutput
{
    /** @var string $Id */
    private $Id;

    /** @var string $Namespace */
    private $Namespace;

    /** @var string $StatusCode */
    private $StatusCode;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param string $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->Namespace;
    }

    /**
     * @param string $Namespace
     */
    public function setNamespace($Namespace)
    {
        $this->Namespace = $Namespace;
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    /**
     * @param string $StatusCode
     */
    public function setStatusCode($StatusCode)
    {
        $this->StatusCode = $StatusCode;
    }
}
