<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class SubscriptionOutput
{
    /** @var integer $Id */
    private $Id;

    /** @var integer $ProductId */
    private $ProductId;

    /** @var string $RequestedVersion */
    private $RequestedVersion;

    /** @var string $RequestedVersionCode */
    private $RequestedVersionCode;

    /** @var string $ActualVersionCode */
    private $ActualVersionCode;

    /** @var integer $Quantity */
    private $Quantity;

    /** @var integer $DataLockCode */
    private $DataLockCode;

    /** @var boolean $Receive */
    private $Receive;

    /** @var string $MarketingClassId */
    private $MarketingClassId;

    /** @var string $MarketingClassDescription */
    private $MarketingClassDescription;

    /** @var null $DeploymentTypes */
    private $DeploymentTypes;

    /** @var integer $ShippingAddressId */
    private $ShippingAddressId;

    /** @var integer $BillingAddressId */
    private $BillingAddressId;

    /** @var string $BillingName */
    private $BillingName;

    /** @var integer $EmailAddressId */
    private $EmailAddressId;

    /** @var string $ChangedDate */
    private $ChangedDate;

    /** @var integer $Relationships */
    private $Relationships;

    /** @var integer $PaymentStatus */
    private $PaymentStatus;

    /** @var integer $SubscriptionPaidId */
    private $SubscriptionPaidId;

    /** @var float $CreditBalance */
    private $CreditBalance;

    /** @var float $Amount */
    private $Amount;

    /** @var string $LastIssueEarnedDescription */
    private $LastIssueEarnedDescription;

    /** @var string $LastIssueEarnedDate */
    private $LastIssueEarnedDate;

    /** @var string $FirstIssueEarnedDescription */
    private $FirstIssueEarnedDescription;

    /** @var string $FirstIssueEarnedDate */
    private $FirstIssueEarnedDate;

    /** @var integer $Term */
    private $Term;

    /** @var integer $IssuesRemaining */
    private $IssuesRemaining;

    /** @var integer $CopiesRemaining */
    private $CopiesRemaining;

    /** @var string $IssueExpirationDate */
    private $IssueExpirationDate;

    /** @var string $OrderDate */
    private $OrderDate;

    /** @var string $OriginalOrderDate */
    private $OriginalOrderDate;

    /** @var string $ExpirationDate */
    private $ExpirationDate;

    /** @var string $DeactivationDate */
    private $DeactivationDate;

    /** @var integer $AutoRenewalCode */
    private $AutoRenewalCode;

    /** @var integer $NumberOfInstallments */
    private $NumberOfInstallments;

    /** @var integer $InstallmentCode */
    private $InstallmentCode;

    /** @var string $Voucher */
    private $Voucher;

    /** @var integer $DonorId */
    private $DonorId;

    /** @var string $GiftMessage */
    private $GiftMessage;

    /** @var string $GiftSentDate */
    private $GiftSentDate;

    /** @var string $VerificationDate */
    private $VerificationDate;

    /** @var integer $VerificationAge */
    private $VerificationAge;

    /** @var integer $Status */
    private $Status;

    /** @var integer $SourceId */
    private $SourceId;

    /** @var string $ClientOrderId */
    private $ClientOrderId;

    /** @var integer $RenewalCount */
    private $RenewalCount;

    /** @var integer $PremiumCode */
    private $PremiumCode;

    /** @var string $PremiumCodeDescription */
    private $PremiumCodeDescription;

    /** @var string $PromoCode */
    private $PromoCode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->ProductId;
    }

    /**
     * @param int $ProductId
     */
    public function setProductId($ProductId)
    {
        $this->ProductId = $ProductId;
    }

    /**
     * @return string
     */
    public function getRequestedVersion()
    {
        return $this->RequestedVersion;
    }

    /**
     * @param string $RequestedVersion
     */
    public function setRequestedVersion($RequestedVersion)
    {
        $this->RequestedVersion = $RequestedVersion;
    }

    /**
     * @return string
     */
    public function getRequestedVersionCode()
    {
        return $this->RequestedVersionCode;
    }

    /**
     * @param string $RequestedVersionCode
     */
    public function setRequestedVersionCode($RequestedVersionCode)
    {
        $this->RequestedVersionCode = $RequestedVersionCode;
    }

    /**
     * @return string
     */
    public function getActualVersionCode()
    {
        return $this->ActualVersionCode;
    }

    /**
     * @param string $ActualVersionCode
     */
    public function setActualVersionCode($ActualVersionCode)
    {
        $this->ActualVersionCode = $ActualVersionCode;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->Quantity;
    }

    /**
     * @param int $Quantity
     */
    public function setQuantity($Quantity)
    {
        $this->Quantity = $Quantity;
    }

    /**
     * @return int
     */
    public function getDataLockCode()
    {
        return $this->DataLockCode;
    }

    /**
     * @param int $DataLockCode
     */
    public function setDataLockCode($DataLockCode)
    {
        $this->DataLockCode = $DataLockCode;
    }

    /**
     * @return bool
     */
    public function isReceive()
    {
        return $this->Receive;
    }

    /**
     * @param bool $Receive
     */
    public function setReceive($Receive)
    {
        $this->Receive = $Receive;
    }

    /**
     * @return string
     */
    public function getMarketingClassId()
    {
        return $this->MarketingClassId;
    }

    /**
     * @param string $MarketingClassId
     */
    public function setMarketingClassId($MarketingClassId)
    {
        $this->MarketingClassId = $MarketingClassId;
    }

    /**
     * @return string
     */
    public function getMarketingClassDescription()
    {
        return $this->MarketingClassDescription;
    }

    /**
     * @param string $MarketingClassDescription
     */
    public function setMarketingClassDescription($MarketingClassDescription)
    {
        $this->MarketingClassDescription = $MarketingClassDescription;
    }

    /**
     * @return null
     */
    public function getDeploymentTypes()
    {
        return $this->DeploymentTypes;
    }

    /**
     * @param null $DeploymentTypes
     */
    public function setDeploymentTypes($DeploymentTypes)
    {
        $this->DeploymentTypes = $DeploymentTypes;
    }

    /**
     * @return int
     */
    public function getShippingAddressId()
    {
        return $this->ShippingAddressId;
    }

    /**
     * @param int $ShippingAddressId
     */
    public function setShippingAddressId($ShippingAddressId)
    {
        $this->ShippingAddressId = $ShippingAddressId;
    }

    /**
     * @return int
     */
    public function getBillingAddressId()
    {
        return $this->BillingAddressId;
    }

    /**
     * @param int $BillingAddressId
     */
    public function setBillingAddressId($BillingAddressId)
    {
        $this->BillingAddressId = $BillingAddressId;
    }

    /**
     * @return string
     */
    public function getBillingName()
    {
        return $this->BillingName;
    }

    /**
     * @param string $BillingName
     */
    public function setBillingName($BillingName)
    {
        $this->BillingName = $BillingName;
    }

    /**
     * @return int
     */
    public function getEmailAddressId()
    {
        return $this->EmailAddressId;
    }

    /**
     * @param int $EmailAddressId
     */
    public function setEmailAddressId($EmailAddressId)
    {
        $this->EmailAddressId = $EmailAddressId;
    }

    /**
     * @return string
     */
    public function getChangedDate()
    {
        return $this->ChangedDate;
    }

    /**
     * @param string $ChangedDate
     */
    public function setChangedDate($ChangedDate)
    {
        $this->ChangedDate = $ChangedDate;
    }

    /**
     * @return int
     */
    public function getRelationships()
    {
        return $this->Relationships;
    }

    /**
     * @param int $Relationships
     */
    public function setRelationships($Relationships)
    {
        $this->Relationships = $Relationships;
    }

    /**
     * @return int
     */
    public function getPaymentStatus()
    {
        return $this->PaymentStatus;
    }

    /**
     * @param int $PaymentStatus
     */
    public function setPaymentStatus($PaymentStatus)
    {
        $this->PaymentStatus = $PaymentStatus;
    }

    /**
     * @return float
     */
    public function getCreditBalance()
    {
        return $this->CreditBalance;
    }

    /**
     * @param float $CreditBalance
     */
    public function setCreditBalance($CreditBalance)
    {
        $this->CreditBalance = $CreditBalance;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->Amount;
    }

    /**
     * @param float $Amount
     */
    public function setAmount($Amount)
    {
        $this->Amount = $Amount;
    }

    /**
     * @return string
     */
    public function getLastIssueEarnedDescription()
    {
        return $this->LastIssueEarnedDescription;
    }

    /**
     * @param string $LastIssueEarnedDescription
     */
    public function setLastIssueEarnedDescription($LastIssueEarnedDescription)
    {
        $this->LastIssueEarnedDescription = $LastIssueEarnedDescription;
    }

    /**
     * @return string
     */
    public function getLastIssueEarnedDate()
    {
        return $this->LastIssueEarnedDate;
    }

    /**
     * @param string $LastIssueEarnedDate
     */
    public function setLastIssueEarnedDate($LastIssueEarnedDate)
    {
        $this->LastIssueEarnedDate = $LastIssueEarnedDate;
    }

    /**
     * @return string
     */
    public function getFirstIssueEarnedDescription()
    {
        return $this->FirstIssueEarnedDescription;
    }

    /**
     * @param string $FirstIssueEarnedDescription
     */
    public function setFirstIssueEarnedDescription($FirstIssueEarnedDescription)
    {
        $this->FirstIssueEarnedDescription = $FirstIssueEarnedDescription;
    }

    /**
     * @return string
     */
    public function getFirstIssueEarnedDate()
    {
        return $this->FirstIssueEarnedDate;
    }

    /**
     * @param string $FirstIssueEarnedDate
     */
    public function setFirstIssueEarnedDate($FirstIssueEarnedDate)
    {
        $this->FirstIssueEarnedDate = $FirstIssueEarnedDate;
    }

    /**
     * @return int
     */
    public function getTerm()
    {
        return $this->Term;
    }

    /**
     * @param int $Term
     */
    public function setTerm($Term)
    {
        $this->Term = $Term;
    }

    /**
     * @return int
     */
    public function getIssuesRemaining()
    {
        return $this->IssuesRemaining;
    }

    /**
     * @param int $IssuesRemaining
     */
    public function setIssuesRemaining($IssuesRemaining)
    {
        $this->IssuesRemaining = $IssuesRemaining;
    }

    /**
     * @return int
     */
    public function getCopiesRemaining()
    {
        return $this->CopiesRemaining;
    }

    /**
     * @param int $CopiesRemaining
     */
    public function setCopiesRemaining($CopiesRemaining)
    {
        $this->CopiesRemaining = $CopiesRemaining;
    }

    /**
     * @return string
     */
    public function getIssueExpirationDate()
    {
        return $this->IssueExpirationDate;
    }

    /**
     * @param string $IssueExpirationDate
     */
    public function setIssueExpirationDate($IssueExpirationDate)
    {
        $this->IssueExpirationDate = $IssueExpirationDate;
    }

    /**
     * @return string
     */
    public function getOrderDate()
    {
        return $this->OrderDate;
    }

    /**
     * @param string $OrderDate
     */
    public function setOrderDate($OrderDate)
    {
        $this->OrderDate = $OrderDate;
    }

    /**
     * @return string
     */
    public function getOriginalOrderDate()
    {
        return $this->OriginalOrderDate;
    }

    /**
     * @param string $OriginalOrderDate
     */
    public function setOriginalOrderDate($OriginalOrderDate)
    {
        $this->OriginalOrderDate = $OriginalOrderDate;
    }

    /**
     * @return string
     */
    public function getExpirationDate()
    {
        return $this->ExpirationDate;
    }

    /**
     * @param string $ExpirationDate
     */
    public function setExpirationDate($ExpirationDate)
    {
        $this->ExpirationDate = $ExpirationDate;
    }

    /**
     * @return string
     */
    public function getDeactivationDate()
    {
        return $this->DeactivationDate;
    }

    /**
     * @param string $DeactivationDate
     */
    public function setDeactivationDate($DeactivationDate)
    {
        $this->DeactivationDate = $DeactivationDate;
    }

    /**
     * @return int
     */
    public function getAutoRenewalCode()
    {
        return $this->AutoRenewalCode;
    }

    /**
     * @param int $AutoRenewalCode
     */
    public function setAutoRenewalCode($AutoRenewalCode)
    {
        $this->AutoRenewalCode = $AutoRenewalCode;
    }

    /**
     * @return int
     */
    public function getNumberOfInstallments()
    {
        return $this->NumberOfInstallments;
    }

    /**
     * @param int $NumberOfInstallments
     */
    public function setNumberOfInstallments($NumberOfInstallments)
    {
        $this->NumberOfInstallments = $NumberOfInstallments;
    }

    /**
     * @return int
     */
    public function getInstallmentCode()
    {
        return $this->InstallmentCode;
    }

    /**
     * @param int $InstallmentCode
     */
    public function setInstallmentCode($InstallmentCode)
    {
        $this->InstallmentCode = $InstallmentCode;
    }

    /**
     * @return string
     */
    public function getVoucher()
    {
        return $this->Voucher;
    }

    /**
     * @param string $Voucher
     */
    public function setVoucher($Voucher)
    {
        $this->Voucher = $Voucher;
    }

    /**
     * @return int
     */
    public function getDonorId()
    {
        return $this->DonorId;
    }

    /**
     * @param int $DonorId
     */
    public function setDonorId($DonorId)
    {
        $this->DonorId = $DonorId;
    }

    /**
     * @return string
     */
    public function getGiftMessage()
    {
        return $this->GiftMessage;
    }

    /**
     * @param string $GiftMessage
     */
    public function setGiftMessage($GiftMessage)
    {
        $this->GiftMessage = $GiftMessage;
    }

    /**
     * @return string
     */
    public function getGiftSentDate()
    {
        return $this->GiftSentDate;
    }

    /**
     * @param string $GiftSentDate
     */
    public function setGiftSentDate($GiftSentDate)
    {
        $this->GiftSentDate = $GiftSentDate;
    }

    /**
     * @return string
     */
    public function getVerificationDate()
    {
        return $this->VerificationDate;
    }

    /**
     * @param string $VerificationDate
     */
    public function setVerificationDate($VerificationDate)
    {
        $this->VerificationDate = $VerificationDate;
    }

    /**
     * @return int
     */
    public function getVerificationAge()
    {
        return $this->VerificationAge;
    }

    /**
     * @param int $VerificationAge
     */
    public function setVerificationAge($VerificationAge)
    {
        $this->VerificationAge = $VerificationAge;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->Status;
    }

    /**
     * @param int $Status
     */
    public function setStatus($Status)
    {
        $this->Status = $Status;
    }

    /**
     * @return int
     */
    public function getSourceId()
    {
        return $this->SourceId;
    }

    /**
     * @param int $SourceId
     */
    public function setSourceId($SourceId)
    {
        $this->SourceId = $SourceId;
    }

    /**
     * @return string
     */
    public function getClientOrderId()
    {
        return $this->ClientOrderId;
    }

    /**
     * @param string $ClientOrderId
     */
    public function setClientOrderId($ClientOrderId)
    {
        $this->ClientOrderId = $ClientOrderId;
    }

    /**
     * @return int
     */
    public function getRenewalCount()
    {
        return $this->RenewalCount;
    }

    /**
     * @param int $RenewalCount
     */
    public function setRenewalCount($RenewalCount)
    {
        $this->RenewalCount = $RenewalCount;
    }

    /**
     * @return int
     */
    public function getPremiumCode()
    {
        return $this->PremiumCode;
    }

    /**
     * @param int $PremiumCode
     */
    public function setPremiumCode($PremiumCode)
    {
        $this->PremiumCode = $PremiumCode;
    }

    /**
     * @return string
     */
    public function getPremiumCodeDescription()
    {
        return $this->PremiumCodeDescription;
    }

    /**
     * @param string $PremiumCodeDescription
     */
    public function setPremiumCodeDescription($PremiumCodeDescription)
    {
        $this->PremiumCodeDescription = $PremiumCodeDescription;
    }

    /**
     * @return string
     */
    public function getPromoCode()
    {
        return $this->PromoCode;
    }

    /**
     * @param string $PromoCode
     */
    public function setPromoCode($PromoCode)
    {
        $this->PromoCode = $PromoCode;
    }

    /**
     * @return int
     */
    public function getSubscriptionPaidId()
    {
        return $this->SubscriptionPaidId;
    }

    /**
     * @param int $SubscriptionPaidId
     */
    public function setSubscriptionPaidId($SubscriptionPaidId)
    {
        $this->SubscriptionPaidId = $SubscriptionPaidId;
    }
}
