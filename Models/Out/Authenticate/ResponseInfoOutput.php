<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-16
 * Time: 10:43 AM
 */

namespace Inovva\omdimp\Models\Out\Authenticate;

class ResponseInfoOutput
{
    /** @var string $EncryptedCustomerId */
    public $EncryptedCustomerId;

    /** @var string $Warning */
    public $Warning;

    /** @var integer $OmedaCustomerId */
    public $OmedaCustomerId;

    /** @var integer $CustomerStatusId */
    public $CustomerStatusId;

    /** @var integer $StatusCode */
    public $StatusCode;

    /** @var string $Success */
    public $Success;
}