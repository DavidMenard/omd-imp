<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-16
 * Time: 10:40 AM
 */

namespace Inovva\omdimp\Models\Out\Authenticate;

class AuthenticationResponseOutput
{
    /** @var ResponseInfoOutput[] $ResponseInfo */
    public $ResponseInfo;

    /** @var string $SubmissionId */
    public $SubmissionId;

}