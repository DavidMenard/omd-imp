<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-26
 * Time: 10:53 AM
 */

namespace Inovva\omdimp\Models\Out;


class PhoneNumberOutput
{
    /** @var int $Id */
    private $Id;

    /** @var integer $PhoneContactType */
    private $PhoneContactType;

    /** @var string $PhoneNumber */
    private $PhoneNumber;

    /** @var string $Extension */
    private $Extension;

    /** @var string $ChangedDate */
    private $ChangedDate;

    /** @var integer $StatusCode */
    private $StatusCode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @param int $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    /**
     * @return int
     */
    public function getPhoneContactType()
    {
        return $this->PhoneContactType;
    }

    /**
     * @param int $PhoneContactType
     */
    public function setPhoneContactType($PhoneContactType)
    {
        $this->PhoneContactType = $PhoneContactType;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->PhoneNumber;
    }

    /**
     * @param string $PhoneNumber
     */
    public function setPhoneNumber($PhoneNumber)
    {
        $this->PhoneNumber = $PhoneNumber;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->Extension;
    }

    /**
     * @param string $Extension
     */
    public function setExtension($Extension)
    {
        $this->Extension = $Extension;
    }

    /**
     * @return string
     */
    public function getChangedDate()
    {
        return $this->ChangedDate;
    }

    /**
     * @param string $ChangedDate
     */
    public function setChangedDate($ChangedDate)
    {
        $this->ChangedDate = $ChangedDate;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->StatusCode;
    }

    /**
     * @param int $StatusCode
     */
    public function setStatusCode($StatusCode)
    {
        $this->StatusCode = $StatusCode;
    }
}
