<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-16
 * Time: 10:40 AM
 */

namespace Inovva\omdimp\Models\Out\Error;

class ErrorResponseOutput
{
    /** @var ErrorOutput[] $Errors */
    public $Errors;

    /** @var string $SubmissionId */
    public $SubmissionId;

}