<?php

namespace Inovva\omdimp;

class Config
{
    const X_OMD_APP_ID  = "__APP_ID__";
    const OMEDA_API_URL = "__URL__";
    const BRAND         = "__BRAND__";
}