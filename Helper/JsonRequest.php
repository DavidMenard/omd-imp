<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-23
 * Time: 3:32 PM
 */

namespace Inovva\omdimp\Helper;


use Exception;
use Inovva\Logger\Logger;
use Inovva\omdimp\Config;
use Inovva\omdimp\Helper\Exception\APIException;

class JsonRequest
{
    static protected $_instance;
    static protected $logger;

    protected $url;

    public static function getInstance(Logger $logger)
    {

        if (!self::$_instance) {
            self::$_instance = new self();
            self::$logger = $logger;
            self::$_instance->url = Config::OMEDA_API_URL;
        }

        return self::$_instance;
    }

    public function call($uri, $data = null, $method = 'GET')
    {
        $uri = $this->url . (strpos($uri, '/', 0) == 0 ? $uri : '/' . $uri);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        $httpHeaders = array();
        $httpHeaders[] = 'Content-Type: application/json';
        $httpHeaders[] = 'x-omeda-appid: ' . Config::X_OMD_APP_ID;
        if ($method != 'GET') {
            $httpHeaders[] = 'Content-length: ' . strlen($data);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeaders);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        switch ($method) {
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, TRUE);
                break;
        }
        if ($method != 'GET') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2000);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2000);

        $response = curl_exec($ch);
        $this->httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

        if ($this->httpStatus == '200'  && empty($body)) {
            return null; // if the response is 200 and the api return an empty body, return immediately otherwise it throws an exception
        }

        $obj = json_decode($body, false);
        $this->_analyzeHttpHeaders($body);

        return $obj;
    }

    protected function _analyzeHttpHeaders($body)
    {
        if ($body == false) {
            throw new Exception('Internal server error', -1);
        }

        $errorCodeFamily = substr($this->httpStatus, 0, 1);
        switch ($errorCodeFamily) {
            case '5':
                throw new Exception('Internal Server Error', 0);
            case '4':
                throw new APIException($body, 0);
        }
    }
}

