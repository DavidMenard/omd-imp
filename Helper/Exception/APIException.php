<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-08-16
 * Time: 10:06 AM
 */

namespace Inovva\omdimp\Helper\Exception;


final class APIException extends \RuntimeException
{
    protected $errorObj;
}