<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-23
 * Time: 7:52 PM
 */

namespace Inovva\omdimp;



use Exception;
use Inovva\Logger\Logger;
use Inovva\omdimp\Helper\JsonMapper;
use Inovva\omdimp\Helper\JsonRequest;
use Inovva\omdimp\Models\Enums\Demographic;
use Inovva\omdimp\Models\Out\CustomerLookupOutput;

class CustomerLookup
{
    /**
     * @param string $usernameID
     * @return CustomerLookupOutput|\WP_Error
     */
        public static function ProcessLookup($usernameID)
    {

        try {
            $customerJSON = JsonRequest::getInstance(Logger::getInstance())->call("/webservices/rest/brand/" . Config::BRAND . "/customer/".$usernameID."/comp/*");
            /** @var CustomerLookupOutput $customerObj */
            $customerObj = JsonMapper::getInstance(Logger::getInstance())->map($customerJSON, new CustomerLookupOutput());
        } catch (Exception $ex) {
            Logger::getInstance()->error("OMEDA Error - Unable to complete the request. Err msg : " . $ex->getMessage());
            return new \WP_Error('wse', 'Unable to complete the request. Err msg : ' . $ex->getMessage());
        }

        $customerObj->setCustomerDemographics(Demographic::sortDemographics($customerObj->getCustomerDemographics(), "DemographicId"));

        return $customerObj;
    }
}