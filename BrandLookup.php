<?php
/**
 * Created by PhpStorm.
 * User: dmenard
 * Date: 2018-07-23
 * Time: 7:52 PM
 */

namespace Inovva\omdimp;


use Exception;
use Inovva\Logger\Logger;
use Inovva\omdimp\Helper\JsonMapper;
use Inovva\omdimp\Helper\JsonRequest;
use Inovva\omdimp\Models\Enums\Demographic;
use Inovva\omdimp\Models\Out\Brand\BrandOutput;

class BrandLookup
{
    /**
     * Retrieve all the configurations and parameters this client has.
     *
     * @return Models\Out\Brand\DemographicOutput[]
     * @throws Exception
     */
    public static function getDemographics()
    {
        $brandJSON = JsonRequest::getInstance(Logger::getInstance())->call("/webservices/rest/brand/" . Config::BRAND . "/comp/*");

        try {
            /** @var BrandOutput $brandObj */
            $brandObj = JsonMapper::getInstance(Logger::getInstance())->map($brandJSON, new BrandOutput());
        } catch (Exception $ex) {
            error_log("OMEDA ERROR -- " . $ex->getMessage());
            throw $ex;
        }

        if (count($brandObj->getDemographics()) < 1) {
            throw new Exception("OMEDA ERROR -- No response received");
        }

        // turn the demographics array to use the demographic key
        return Demographic::sortDemographics($brandObj->getDemographics());
    }
}